<!-- description -->

Upstream issue: XYZ

### Steps to fix:

```bash
cd ~/jhbuild/checkout/XYZ/

curl XYZ.patch | git apply -v

echo -n "
SHORT PATCH
" | git apply -v
```

1. Enter XYZ source directory
2. Apply patch XYZ

---

**Note:** If you already built (or tried to build) XYZ, rebuild using the following command after applying the fix:

```bash
jhbuild buildone -cfn XYZ
```
/label ~"Fix: Patch"
