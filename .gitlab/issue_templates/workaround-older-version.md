<!-- description -->

Upstream issue: XYZ

### Steps to fix:

```bash
cd ~/jhbuild/checkout/XYZ/
git checkout XYZ
```

1. Enter XYZ source directory
2. Checkout commit XYZ

---

**Note:** If you already built (or tried to build) XYZ, rebuild using the following command after applying the fix:

```bash
jhbuild buildone -cfn XYZ
```
/label ~"Fix: Revert"
